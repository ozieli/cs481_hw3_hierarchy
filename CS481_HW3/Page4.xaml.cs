﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

// CS481 HW3 - TabbedPage Game Page4 CS
// Feb 20, 2020
// Olaf Zielinski

namespace CS481_HW3
{
    public partial class Page4 : ContentPage
    {
        public Page4()
        {
            InitializeComponent();
        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        public void ClickedBack(object sender, EventArgs e) //button sends you to page 2
        {

            Navigation.PushAsync(new Page1());
        }

    }
}
