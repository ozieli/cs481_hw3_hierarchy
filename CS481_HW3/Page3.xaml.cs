﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

// CS481 HW3 - TabbedPage Game Page3 CS
// Feb 20, 2020
// Olaf Zielinski

namespace CS481_HW3
{
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }

        public void ClickedBack(object sender, EventArgs e) //button sends you to page 2
        {

            Navigation.PushAsync(new Page1());
        }

    }
}
