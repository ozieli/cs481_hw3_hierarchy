﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

// CS481 HW3 - TabbedPage Game Page1 CS
// Feb 20, 2020
// Olaf Zielinski

namespace CS481_HW3
{
    public partial class Page1 : ContentPage
    {
        int count = 0; // Counter keeps track of how many times the wait button has been clicked.
        int status = 0; // The status variable keeps track of the status of the baby. 

        public Page1()
        {
            InitializeComponent();
        }

        async void Button_Clicked_1(object sender, EventArgs e)
        {
            var secondPage = new Page2();
            secondPage.BindingContext = status;
            await Navigation.PushAsync (secondPage);
        }

        async void OnAlertYesNoClicked(object sender, EventArgs e)
        {
            bool answer = await DisplayAlert("Are you sure?", "Are you going to leave a baby alone?", "Sure", "Nah");

            if(answer)
            {
                await Navigation.PushAsync(new Page2());
            }
        }


        async void Button_Clicked_2(object sender, EventArgs e) //button sends you to page 3
        {
            await DisplayAlert("Alert", "You have tripped!", "Look at the baby");

            await Navigation.PushAsync(new Page3());
        }

        public void Button_Clicked_3(object sender, EventArgs e) //button sends you to page 3
        {
            count++;

            if (count >= 3)
            {
                BackgroundColor = Xamarin.Forms.Color.Beige;
                windowimage.Source = "Asset1.png";
                textheader.TextColor = Xamarin.Forms.Color.Black; 
            }

            if (count >= 5)
            { 
            Navigation.PushAsync(new Page4());
            }
        }

    }
}
