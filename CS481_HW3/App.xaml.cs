﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CS481_HW3.Services;
using CS481_HW3.Views;

// CS481 HW3 - TabbedPage Game App CS
// Feb 20, 2020
// Olaf Zielinski

namespace CS481_HW3
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new NavigationPage(new Page1());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
