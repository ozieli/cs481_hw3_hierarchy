﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

// CS481 HW3 - TabbedPage Game Page2 CS
// Feb 20, 2020
// Olaf Zielinski

namespace CS481_HW3
{
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }

        public void ClickedBack(object sender, EventArgs e) //button sends you to page 2
        {

            Navigation.PushAsync(new Page1());

        }

        protected override void OnAppearing()
        {
            DisplayAlert("Alert", "The baby woke up!", "Look at the baby");
        }

        protected override void OnDisappearing()
        {
            DisplayAlert("Alert", "But the baby is still crying!", "Just let it cry.");
        }

    }
}
